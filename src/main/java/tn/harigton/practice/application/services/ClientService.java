package tn.harigton.practice.application.services;

import org.springframework.stereotype.Service;
import tn.harigton.practice.infrastructure.repositories.Entity.ClientEntity;
import tn.harigton.practice.infrastructure.repositories.ClientRepository;

import java.util.List;

@Service
public class ClientService {

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

     public void addClient(ClientEntity clientEntity){
        clientRepository.save(clientEntity);
    }
     public void deleteClient(ClientEntity clientEntity){
        clientRepository.deleteById(clientEntity.getId());
    }

    public List<ClientEntity> getAllClient(){return clientRepository.findAll();}
    public ClientEntity getClientById(int id){
        return clientRepository.findById(id).orElse(null);
    }

    public List<ClientEntity> getClientByName(String name){
        return clientRepository.findByNameIs(name);
    }

    public void updateClient(int id, ClientEntity clientEntity){
        ClientEntity clientEntity1 = clientRepository.findById(id).orElse(null);
        assert clientEntity1 != null;
        clientEntity1.setName(clientEntity.getName());
        clientEntity1.setEmail(clientEntity.getEmail());
        clientEntity1.setPassword(clientEntity.getPassword());
        clientEntity1.setCountry(clientEntity.getCountry());
        clientRepository.save(clientEntity1);
    }

}
