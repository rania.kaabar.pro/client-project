package tn.harigton.practice.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.harigton.practice.infrastructure.repositories.Entity.ClientEntity;

import java.util.List;

public interface ClientRepository extends JpaRepository<ClientEntity, Integer> {

    List<ClientEntity> findByNameIs(String name);
}
