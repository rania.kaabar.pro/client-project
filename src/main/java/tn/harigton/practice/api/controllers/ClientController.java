package tn.harigton.practice.api.controllers;

import org.springframework.web.bind.annotation.*;
import tn.harigton.practice.infrastructure.repositories.Entity.ClientEntity;
import tn.harigton.practice.application.services.ClientService;

import java.util.List;
@CrossOrigin("*")
@RestController("/api/v1")
public class ClientController {

    private final ClientService clientService;


    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping("/add-client")
    void addClient(@RequestBody ClientEntity clientEntity){
 clientService.addClient(clientEntity);
    }

    @DeleteMapping()
    void deleteClient(@RequestBody ClientEntity clientEntity){
        clientService.deleteClient(clientEntity);
    }

    @GetMapping("/get-by-id")
    ClientEntity getClientById(@PathVariable int id){
       return clientService.getClientById(id);
    }

    @GetMapping("/get-by-name/{name}")
    List<ClientEntity> getClientByName(@PathVariable("name") String name){
        return clientService.getClientByName(name);
    }
@GetMapping("/getAllClient")
List<ClientEntity> getAllClient(){
        return clientService.getAllClient();
}
    @PutMapping("/{id}")
    void updateClient(@PathVariable("id") int id,@RequestBody ClientEntity clientEntity){
        clientService.updateClient(id, clientEntity);    }
}



